**Purpose:**
<br>
Personal

**keywords:** 
<br>
Spring, Hibernate, JavaFX, Interface/Abstract

**How the program functions:**
<br>
Pretty much self-explanatory but it will be tricky for me to create such a program which will also draw a good graph. I want to use JavaFX to draw a graph on the fly as well as to enlarge the person and show details when hovering over a photo.